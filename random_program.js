const { fetchRandomNumbers, fetchRandomString } = require("./random.js");
const fs = require("fs");

/**
 * The function fetchRandomNumbers can be used by passing a callback,
 */
async function fetchRandomNumbersFunction() {
  try {
    const fetchRandomNumbers_value = await fetchRandomNumbers();
    console.log("fetchRandomNumbers : " + fetchRandomNumbers_value);
  } catch (error) {
    console.log(error);
  }
}
fetchRandomNumbersFunction();

/**
 * The function fetchRandomString can be used by passing a callback,
 */
async function fetchRandomStringFunction() {
  try {
    const fetchRandomString_value = await fetchRandomString();
    console.log("fetchRandomString : " + fetchRandomString_value);
  } catch (error) {
    console.log(error);
  }
}
fetchRandomStringFunction();

/**
 * Fetch a random number -> add it to a sum variable and print sum-> fetch another random
 * variable-> add it to the same sum variable and print the sum variable.
 */
async function fetchRandomNumbersSum() {
  let fetch_sum = 0;
  try {
    const fetchRandomNumbers_sum_value1 = await fetchRandomNumbers();
    fetch_sum += fetchRandomNumbers_sum_value1;
    const fetchRandomNumbers_sum_value2 = await fetchRandomNumbers();
    fetch_sum += fetchRandomNumbers_sum_value2;
    console.log("fetch_sum : " + fetch_sum);
  } catch (error) {
    console.log(error);
  }
}
fetchRandomNumbersSum();

/**
 * Fetch a random number and a random string simultaneously, concatenate
 * their and print the concatenated string
 */
async function stringNumberConcatenate() {
  try {
    const concatenateValue =
      (await fetchRandomNumbers()) + (await fetchRandomString());
    console.log("concatenateValue : " + concatenateValue);
  } catch (error) {
    console.log(error);
  }
}
stringNumberConcatenate();

/**
 * Fetch 10 random numbers simultaneously -> and print their sum.
 */
//using promise
let promiseArray = [];
for (let index = 0; index < 10; index++) {
  promiseArray.push(fetchRandomNumbers());
}
let array = Promise.all(promiseArray);
array
  .then((data) => {
    const promiseSum = data.reduce((sum, curr) => sum + curr);
    console.log(promiseSum);
    return promiseSum;
  })
  .catch((error) => console.error(error));


  //using async function
async function simultaneouslyFetchNumberSum(fetchRandomNumbers, fetchNumber) {
  try {
    let asyncArray = [];
    for (let index = 0; index < fetchNumber; index++) {
      asyncArray.push(fetchRandomNumbers());
    }
    console.log("printing", asyncArray);
    const asyncArrayValue = await Promise.all(asyncArray);
    let asyncSum = asyncArrayValue.reduce((sum, val) => {
      return sum + val;
    });
    console.log("asyncSum=" + asyncSum);
  } catch (error) {
    console.error(error);
  }
}

simultaneouslyFetchNumberSum(fetchRandomNumbers, 10);
