
const fs = require('fs');
const { getBoard, getLists, getCards } = require('./trello-callbacks');

// Task 1 board -> lists -> cards for list qwsa221
function boardCallback1(board1) {
    return getLists(board1.id, getListsCallback1);
}

function getListsCallback1(listId1) {
    return getCards(listId1[0].id, getCardsCallback1);
}

function getCardsCallback1(listIdValue) {
    console.log(listIdValue);
}

getBoard(boardCallback1);



// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
function boardCallback2(board2) {
    return getLists(board2.id, getListsCallback2);
}

function getListsCallback2(listId2) {
    getCards(listId2[0].id,getCardsCallback2);
    getCards(listId2[1].id,getCardsCallback2);
}

function getCardsCallback2(listId1) {
    console.log(listId1);
}

getBoard(boardCallback2);


// Task 3 board -> lists -> cards for all lists simultaneously

function boardCallback3(board2) {
    return getLists(board2.id, getListsCallback3);
}

function getListsCallback3(listId2) {
    listId2.forEach((listId)=>{
        getCards(listId.id,getCardsCallback3);
    })
}

function getCardsCallback3(listId1) {
        console.log( " ********************************")
        console.log(listId1);
    
}

getBoard(boardCallback3);
