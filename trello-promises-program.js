const fs = require("fs");
const { getBoard, getLists, getCards } = require("./trello-promises.js");

// Task 1 board -> lists -> cards for list qwsa221

getBoard()
  .then((board) => {
    return board.id;
  })
  .catch((err) => {
    console.log(err);
  })
  .then((boardId) => {
    return getLists(boardId).then((listValue) => {
      return listValue[0].id;
    });
  })
  .then((listId) => {
    return getCards(listId).then((value) => {
      console.log(value);
    });
  })
  .catch((err) => {
    console.log(err);
  });
// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously

getBoard()
  .then((board) => {
    return board.id;
  })
  .catch((err) => {
    console.log(err);
  })
  .then((boardId) => {
    return getLists(boardId).then((list_value) => {
      var listObject = {};
      listObject.data1 = list_value[0].id;
      listObject.data2 = list_value[1].id;
      return listObject;
    });
  })
  .then((idValue) => {
    let promiseArray = [];
    promiseArray.push(getCards(idValue.data1));
    promiseArray.push(getCards(idValue.data2));
    let value = Promise.all(promiseArray);
    return value;
  })
  .then((value) => {
    console.log(value);
  })
  .catch((err) => {
    console.log(err);
  });

// Task 3 board -> lists -> cards for all lists simultaneously

getBoard()
  .then((board) => {
    return board.id;
  })
  .catch((err) => {
    console.log(err);
  })
  .then((boardId) => {
    return getLists(boardId).then((list_value) => {
      var listObject = {};
      for (let i = 0; i < list_value.length; i++) {
        listObject[i] = list_value[i].id;
      }
      return listObject;
    });
  })
  .then((idValue) => {
    let promiseArray = [];
    for (let key in idValue) {
      promiseArray.push(getCards(idValue[key]));
    }
    let value = Promise.all(promiseArray);
    return value;
  })
  .then((value) => {
    console.log(value);
  })
  .catch((err) => {
    console.log(err);
  });

// let promiseArray = [];
// for (let index = 0; index < 10; index++) {
//   promiseArray.push(fetchRandomNumbers());
// }
// let array = Promise.all(promiseArray);
// array
//   .then((data) => {
//     const promiseSum = data.reduce((sum, curr) => sum + curr);
//     console.log(promiseSum);
//     return promiseSum;
//   })
//   .catch((error) => console.error(error));
